from rest_framework import serializers

from salary_survey.models import SalarySurvey


class SalarySurveySerializer(serializers.ModelSerializer):
    class Meta:
        model = SalarySurvey
        fields = '__all__'
        # fields = ('id',
        #           'timestamp',
        #           'age',
        #           'industry',
        #           'job',
        #           'annual_salary',
        #           'currency',
        #           'location',
        #           'education',
        #           'additional',
        #           'other_currency',
        #           )
