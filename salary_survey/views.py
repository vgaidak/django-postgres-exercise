# Create your views here.
from rest_framework import viewsets

from .models import SalarySurvey
from .serializers import SalarySurveySerializer


class SalarySurveyViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = SalarySurveySerializer
    queryset = SalarySurvey.objects.all().order_by('timestamp')
