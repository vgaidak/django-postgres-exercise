# Create your models here.
from django.db import models


class SalarySurvey(models.Model):
    timestamp = models.CharField(max_length=100, blank=False, default='')
    age = models.CharField(max_length=50, default='')
    industry = models.CharField(max_length=512, default='')
    job = models.CharField(max_length=500, default='')
    annual_salary = models.CharField(max_length=512, default='')
    currency = models.CharField(max_length=100, default='')
    location = models.CharField(max_length=500, default='')
    education = models.CharField(max_length=500, default='')
    additional = models.CharField(max_length=2000, default='')
    other_currency = models.CharField(max_length=512, default='')
