from django.apps import AppConfig


class SalarySurveyConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'salary_survey'
