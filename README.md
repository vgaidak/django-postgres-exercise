![img.png](img.png)

![img_1.png](img_1.png)

![img_2.png](img_2.png)

# RUN
`python3 manage.py runserver`

# WHAT WAS DONE
REST API with data from salary_survey1.csv as a Postgres db table
API returns row by id

# NOTES

This API doesn't meet the listed requirements: filtering and sorting doesn't work; it is not read-only; just one schema was created.
Things above took slightly more than 3 hours.

It was hard for me cause I'm completely newbie at Django, but very interesting too.
Going to finish this exercise after accomplishing some course on Django.

Thanks for motivation!


https://github.com/pineapplehq/hiring-exercises/blob/master/backend/instructions.md